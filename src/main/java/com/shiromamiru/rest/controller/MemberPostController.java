package com.shiromamiru.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shiromamiru.common.dto.MemberPostDto;
import com.shiromamiru.common.service.PostService;

@Controller
@RequestMapping("/memberPost")
public class MemberPostController {
	
	@Autowired
	private PostService postService;
	
	@RequestMapping(value = "/{gplusId}", method = RequestMethod.GET)
	@ResponseBody
	public MemberPostDto getMemberPosts(@PathVariable String gplusId) {
		return postService.getMemberPostList(gplusId);
	}
}
