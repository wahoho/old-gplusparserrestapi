package com.shiromamiru.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shiromamiru.common.dto.PostDto;
import com.shiromamiru.common.service.PostService;

@Controller
@RequestMapping("/post")
public class PostController {
	
	@Autowired
	private PostService postService;

	
	@RequestMapping(value = "/{gplusId}/{postId}", method = RequestMethod.GET)
	@ResponseBody
	public PostDto getPost(@PathVariable String gplusId, @PathVariable String postId) {
		return postService.getPost(gplusId, postId);
	}
	
	@RequestMapping(value = "/deleteall/{gplusId}", method = RequestMethod.GET)
	public void deleteAllPost(@PathVariable String gplusId) {
		postService.deleteAllPost(gplusId);
	}
}
